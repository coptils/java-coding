package com.sda.fundamentals.lamp;

public class Lamp {
    private boolean isOn;

    private void turnOn(){
        isOn=true;
        System.out.println("Light on? " + isOn);
    }

    private static void turnOff(){
      //  isOn=false;
        System.out.println("Light on? " + false);
    }

    public static void main(String[] args) {
        Lamp lampa = new Lamp(); // o instanta/obiect al clasei Lamp
        lampa.turnOn(); // apel al metodei non-statice
        Lamp.turnOff(); // apel al metodei statice
        turnOff(); // apel al metodei statice (se poate deoarece suntem in interiorul clasei in care este definita si metoda)
    }
}
