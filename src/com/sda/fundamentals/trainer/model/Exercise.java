package com.sda.fundamentals.trainer.model;

public class Exercise {
    private int id;
    private int staminaConsumed;
    private int strengthConsumed;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStaminaConsumed() {
        return staminaConsumed;
    }

    public void setStaminaConsumed(int staminaConsumed) {
        this.staminaConsumed = staminaConsumed;
    }

    public int getStrengthConsumed() {
        return strengthConsumed;
    }

    public void setStrengthConsumed(int strengthConsumed) {
        this.strengthConsumed = strengthConsumed;
    }
}
