package com.sda.fundamentals.trainer.model;

public class Trainer {
    private String name;

    private Trainee trainee;

    public Trainer(String name) {
        this.name = name;
    }

    public Trainee getTrainee() {
        return trainee;
    }

    public void setTrainee(Trainee trainee) {
        this.trainee = trainee;
    }

    @Override
    public String toString() {
        return name;
    }
}
