package com.sda.fundamentals.trainer.model;

public class Trainee {
    private String name;
    private int stamina;
    private int strength;

    public Trainee(String name) {
        this.name = name;
        this.stamina = 100;
        this.strength = 100;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStamina() {
        return stamina;
    }

    public boolean setStamina(int stamina) {
        if (stamina >= 0) {
            this.stamina = stamina;
            return true;
        }

        System.out.println("Stamina below 0! Please consider adding more supplements to increase it.");
        return false;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public void addExercise(Exercise exercise) {
        boolean staminaUpdated = this.setStamina(this.getStamina() + exercise.getStaminaConsumed());
        if (staminaUpdated) {
            this.setStrength(this.getStrength() + exercise.getStrengthConsumed());
        }
        System.out.println("The exercise with id: " + exercise.getId() + " has been completed successfully!");
    }

    public void addSupplements(Supplement supplement) {
        this.setStamina(this.getStamina() + supplement.getStaminaAdded());
        this.setStrength(this.getStrength() + supplement.getStrengthAdded());
    }

    @Override
    public String toString() {
        return "Trainee{" +
                "name='" + name + '\'' +
                ", stamina=" + stamina +
                ", strength=" + strength +
                '}';
    }
}
