package com.sda.fundamentals.trainer.model;

public class Supplement {
    private int id;
    private int staminaAdded;
    private int strengthAdded;

    public Supplement(int id, int staminaAdded, int strengthAdded) {
        this.id = id;
        this.staminaAdded = staminaAdded;
        this.strengthAdded = strengthAdded;
    }

    public int getStaminaAdded() {
        return staminaAdded;
    }

    public int getStrengthAdded() {
        return strengthAdded;
    }
}
