package com.sda.fundamentals.trainer.service;

import com.sda.fundamentals.trainer.model.Exercise;
import com.sda.fundamentals.trainer.model.Supplement;
import com.sda.fundamentals.trainer.model.Trainee;
import com.sda.fundamentals.trainer.model.Trainer;

import java.util.Random;

/**
 * Personal Trainer
 * Create class Trainee, it should contain fields like: name, stamina, strength.
 * You’ll simulate both sides – Trainer and Trainee. Within a while loop you will be
 * asked for an exercise to be done. Every exercise should add/reduce
 * stamina/strength.
 * Take into account that stamina should not be reduced below 0.
 * Consider adding some supplements that will recover the stamina. Supplement
 * should be additional class.
 */
public class TrainerSimulatorService {
    private static final Random RANDOM = new Random();

    public static void main(String[] args) {

        Trainer trainer = new Trainer("Alex");
        trainer.setTrainee(new Trainee("Simona"));
        Exercise exercise = new Exercise();

        int i = 0;
        while (i < 5) {
            exercise.setId(RANDOM.nextInt(1, 10));
            exercise.setStaminaConsumed(RANDOM.nextInt(-50, 1));
            exercise.setStrengthConsumed(RANDOM.nextInt(-5, 5));

            System.out.println("Please complete the exercise with id: " + exercise.getId());
            trainer.getTrainee().addExercise(exercise);

            if (trainer.getTrainee().getStamina() < 5) {
                trainer.getTrainee().addSupplements(new Supplement(RANDOM.nextInt(1, 10), RANDOM.nextInt(10, 50), RANDOM.nextInt(10, 100)));
            }
            i++;
        }

    }
}
