package com.sda.fundamentals.circle;

/**
 * Writes a class named Circle to represent a circle that has two properties:
 * radius (of type double) and color (of type String).
 * The class properties should not be accessible from outside the class (class encapsulation).
 * <p>
 * The radius and color should have default values, 1.0 and "red", respectively.
 * The Circle class should have two overloaded constructors.
 * One constructor with no arguments (which sets both radius and color to default).
 * And one constructor with given radius, but color default.
 * The Circle class should have two public method:
 * first method will allow us to retrieve radius;
 * and the second method will allow us to retrieve area of the circle;
 * 1. Modify the class Circle to include a third constructor for constructing a Circle instance with the given radius and color.
 * 2. Modify the test program TestCircle to construct an instance of Circle using this constructor.
 * 3. Add a getter for variable color for retrieving the color of a Circle instance.
 * 4. Modify the test program to test this method.
 * 5. Is there a need to change the values of radius and color of a Circle instance after it is constructed? If so, add two public methods called setters for changing the radius and color of a Circle instance.
 * 6. Modify the TestCircle to test these methods
 * 7. Implement a toString() method and test it;
 */
public class Circle {
    // private instance variable, not accessible from outside this class
    private double radius;
    private String color;

    // 1st constructor, which sets both radius and color to default
    public Circle() {
        this.radius = 1.0d;
        color = "red";
    }

    // 2nd constructor with given radius, but color default
    public Circle(double raza) {
        this.radius = raza;
        color = "red";
    }

    public Circle(double raza, String culoare) {
        this.radius = raza;
        this.color = culoare;
    }

    public static String retrieveCercName() {
        return "Clasa Cerc";
    }

    // A public method for computing the area of circle
    public double retrieveArea() {
        return Math.PI * radius * radius;
    }

    public void setRadius(double raza) {
        this.radius = raza;
    }

    public void setColor(String culoare) {
        this.color = culoare;
    }

    // A public method for retrieving the radius
    public double getRadius() {
        return radius;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "Cercul are raza: " + this.getRadius() + " si culoarea: " + this.getColor();
    }
}
