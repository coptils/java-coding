package com.sda.fundamentals.circle;

public class CircleTest {
    public static void main(String[] args) {

        // Declare and allocate an instance of class Circle called cerc1
        //  with default radius and color
        Circle cerc1 = new Circle();
        // cerc1.color = "rosu";
        // cerc1.radius = 13.4;
        //  System.out.println(cerc1.color);
        //  System.out.println(cerc1.radius);

        // Use the dot operator to invoke methods of instance cerc1.
        System.out.println(cerc1.getColor());
        System.out.println(cerc1.getRadius());

        // Declare and allocate an instance of class circle called cerc2
        //  with the given radius and default color
        Circle cerc2 =  new Circle(13.4214d);
        // Use the dot operator to invoke methods of instance cerc2.
        System.out.println("Atribute cerc2: " + cerc2.getRadius() + "  " + cerc2.getColor());

        Circle cerc3 =  new Circle(5.1, "blue");
        System.out.println("Atribute cerc3: " + cerc3.getRadius() + "  " + cerc3.getColor());

        cerc3.setColor("White");
        cerc3.setRadius(10);

        double areaCerc3 =  cerc3.retrieveArea();
        System.out.println("Aria cerc 3: " + areaCerc3);

        System.out.println("Nume clasa: " + Circle.retrieveCercName());

        // cerc3.toString() =>
        //
        System.out.println(cerc3); // apel implicit cand e pasata o instanta (cerc3) lui println
        System.out.println(cerc3.toString()); // apel explicit al metodei toString()
        System.out.println("Cerc 3: " + cerc3); // '+' invokes toString() too on cerc3 object
    }

}
