package com.sda.fundamentals.poem;
/**
 * Write an application that consists of few classes:
 * a. Author class, representing an author – poem writer, which consists of fields surname and nationality (both of type String)
 * b. Poem class, representing poem, which consists of fields creator (type Author) and stropheNumbers (type int – numbers of strophes in poem)
 * c. Main class, with main method, inside which you will:
 *  i. Create three instances of Poem class, fill them with data (using constructor and/or setters) and store them in array
 *  ii. Write a surname of an author, that wrote a longest poem (let your application calculate it!)
 */
public class Autor {
    private String surname;
    private String nationality;

    // metoda auto-generate => asa putem seta valorile field-urilor
    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;

    }

    // metoda auto-generata (shift-insert) => asa putem citi valorile field-urilor
    public String getSurname() {
        return surname;
    }
}
