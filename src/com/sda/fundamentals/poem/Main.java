package com.sda.fundamentals.poem;

import java.sql.SQLOutput;

public class Main {
    public static void main(String[] args) {
        Autor autor1 = new Autor(); // construim un obiect din clasa Autor, fara sa avem valori pentru field-urile din clasa
        // acum setam valori pentru fieldurile din clasa, folosind instanta creata, autor1
        autor1.setSurname("Eminescu");
        autor1.setNationality("Roman");
        Autor autor2 = new Autor();
        autor2.setSurname("Bacovia");
        autor2.setNationality("Roman");
        Autor autor3 = new Autor();
        autor3.setSurname("Arghezi");
        autor3.setNationality("Roman");

        Poem poem1 = new Poem(autor1,100); // construim un obiect cu field-urile deja setate, nu mai avem nevoie sa le setam cu set-ers
        Poem poem2 = new Poem(autor2,60);
        Poem poem3 = new Poem(autor3,50);


        Poem[] poems = new Poem[]{poem1, poem2, poem3};
        Poem poem = poem1;
        for (Poem element : poems ) {
            if (poem.getStrofeNr()< element.getStrofeNr()){
                poem = element;
            }

        }
        System.out.println("Autorul poemului cu cel mai mare nr. de strofe este:"+ poem.getAutor().getSurname());


    }
}
