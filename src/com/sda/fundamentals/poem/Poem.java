package com.sda.fundamentals.poem;

public class Poem {
    private Autor autor;
    private int strofeNr;

    public Poem(Autor autor1, int strofeNr1) {
        this.autor = autor1;
        this.strofeNr = strofeNr1;

    }

    public Autor getAutor() {
        return autor;
    }

    public int getStrofeNr() {
        return strofeNr;
    }
}
