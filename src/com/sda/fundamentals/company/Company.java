package com.sda.fundamentals.company;

import java.util.Arrays;

public class Company {
    private String name;
    private Employee[] employees;

    public Company(String name) {
        this.name = name;
    }

    public void setEmployees(Employee[] employees) {
        this.employees = employees;
    }

//    public double getAverageAge() {
//        double sum = 0.0d;
//        for (int i = 0; i < employees.length; i++) {
//            sum = sum + employees[i].getAge();
//        }
//        double avg = sum / employees.length;
//        return avg;
//    }

    public double getAverageAge() {
        double sum = 0.0d;
        for (Employee employee: employees) { // ia pe rand fiecare element din array atat timp cat exista elemente in array; parcurge array-ul de element de la primul la ultimul
            sum += employee.getAge();
        }
        return sum/employees.length;
    }


    @Override
    public String toString() {  // metoda folosita doar pentru afisarea datelor
        return "Company{" +
                "name='" + name + '\'' +
                ", employees=" + Arrays.toString(employees) +
                '}';
    }
}
