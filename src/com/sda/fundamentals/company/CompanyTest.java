package com.sda.fundamentals.company;

public class CompanyTest {
    public static void main(String[] args) {
//        Employee employee1 = new Employee("Stefan", "Popescu", 64);
//        Employee employee2 = new Employee("Fanica", "Ionescu", 24);
        Employee employee3 = new Employee("Stelian", "Gigescu", 53);
        Employee employee4 = new Employee("Petru", "Pavelescu", 34);
        Employee employee5 = new Employee("Emil", "Sir", 61);

        Employee[] employees = new Employee[]
                {buildEmployees("Stefan", "Popescu", 64),
                        buildEmployees("Fanica", "Ionescu", 24)
                };

        Company company = new Company("SDA");

        company.setEmployees(employees);

        System.out.println(company.toString());
        System.out.println("Varsta medie a angajatilor din companie este: " + company.getAverageAge());


        Company company2 = new Company("SRL-ul meu");

//        Employee employee6 = new Employee("Stefan2", "Popescu", 64);
//        Employee employee7 = new Employee("Fanica2", "Ionescu", 24);
//        Employee employee8 = new Employee("Stelian2", "Gigescu", 53);
//        Employee employee9 = new Employee("Petru2", "Pavelescu", 34);
//        Employee employee10 = new Employee("Emil2", "Sir", 61);

        Employee[] employees2 = new Employee[]{buildEmployees("Fanica2", "Ionescu", 24),
                new Employee("Stefan", "popewscu", 52)
        };

        company2.setEmployees(employees2);

    }

    public static Employee buildEmployees(String firstName, String lastName, int age) {
        return new Employee(firstName, lastName, age);
    }
}
