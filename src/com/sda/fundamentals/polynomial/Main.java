package com.sda.fundamentals.polynomial;

/**
 * ax^b = monom
 * A method getDegree() that returns the degree of this polynomial.
 * A method toString() that returns "cnx^n+cn-1x^(n-1)+...+c1x+c0".
 * A method evaluate(double x) that evaluate the polynomial for the given x, by substituting the given x into the polynomial expression.
 * Methods add() and multiply() that adds and multiplies this polynomial with the given MyPolynomial instance another, and returns this instance that contains the result.
 */
public class Main {
    public static void main(String[] args) {
        Polynomial polynomial = new Polynomial(3, 2, 1);

        System.out.println(polynomial);
        System.out.println(polynomial.evaluate(2));
        System.out.println(polynomial.add(new Polynomial(3, 2, 1)));

        Polynom p1 =  new Polynom(new Monom(3, 2), new Monom(2, 1), new Monom(1, 0));
        Polynom p2 =  new Polynom(new Monom(3, 2), new Monom(2, 1), new Monom(1, 0));
        System.out.println(p1);
        System.out.println(p2);

        System.out.println(p1.add(p2));
        System.out.println(p2.evaluate(2));
    }
}
