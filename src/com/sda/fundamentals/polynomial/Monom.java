package com.sda.fundamentals.polynomial;

public class Monom {
    private double coefficient;
    private int degree;

    public Monom(double coefficient, int degree) {
        this.coefficient = coefficient;
        this.degree = degree;
    }

    public double getCoefficient() {
        return coefficient;
    }

    public int getDegree() {
        return degree;
    }

    public void add(Monom m) {
        if (this.degree == m.degree) {
            this.coefficient += m.coefficient;
        }
    }

    @Override
    public String toString() {
        if (this.getCoefficient() == 0) {
            return "";
        }

        String ans = "";

        if (degree == 0) {
             ans = Double.toString(this.getCoefficient());
        } else {
            ans = this.getCoefficient() + "X^" + degree;
        }

        return ans;
    }
}
