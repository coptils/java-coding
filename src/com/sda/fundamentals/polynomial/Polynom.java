package com.sda.fundamentals.polynomial;

import java.util.Arrays;

public class Polynom {

    private Monom[] monoms;

    public Polynom(Monom... monoms) {
        this.monoms = monoms;
    }

    public Monom[] getMonoms() {
        return monoms;
    }

    public int getDegree() {
        return monoms.length - 1;
    }

    public Polynom add(Polynom polynom) {
        for (Monom m1 : this.getMonoms()) {
            for (Monom m2 : polynom.getMonoms()) {
                if (m1.getDegree() == m2.getDegree()) {
                    m1.add(m2);
                }
            }
        }

        return this;
    }

    public double evaluate(int x) { // a^x2
        double result = 0.0d;

        for (Monom monom : monoms) {
            result += monom.getCoefficient() * Math.pow(x, monom.getDegree());
        }

        return result;
    }

    @Override
    public String toString() {
        return "Polynom{" +
                "monoms=" + Arrays.toString(monoms) +
                '}';
    }
}
