package com.sda.fundamentals.polynomial;

public class Polynomial {

    private double[] coefficients;

    public Polynomial(double... coefficients) {
        this.coefficients = coefficients;
    }

    public double[] getCoefficients() {
        return coefficients;
    }

    public int getDegree() {
        return coefficients.length - 1;
    }

    public double evaluate(double x) {
        double sum = 0.0;

        for (int i = getDegree(); i >= 0; i--) {
            sum += coefficients[i] * Math.pow(x, i);
        }

        return sum;
    }

    public Polynomial add(Polynomial polynomial) {
        if (this.getDegree() == polynomial.getDegree()) {
            for (int i = this.getDegree(); i >= 0; i--) {
                double newCoeff = this.coefficients[i] + polynomial.getCoefficients()[i];
                this.coefficients[i] = newCoeff;
            }
        }

        return this;
    }

    public Polynomial multiply(Polynomial polynomial) {
        // to be added
        return this;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();

        for (int i = getDegree(); i >= 0; i--) {
            if (i == 1) {
                str.append(coefficients[i]);
                str.append("x");
                str.append("+");
            } else if (i == 0) {
                str.append(coefficients[i]);
            } else {
                str.append(coefficients[i]);
                str.append("x^");
                str.append(i);
                str.append("+");
            }
        }

        return str.toString();
    }
}
