package com.sda.fundamentals.agenda;

public class AgendaTest {
    public static void main(String[] args) {
        Agenda agenda = new Agenda();
        agenda.addNumbers("0736146756");
        agenda.addNumbers("0736146746");
        agenda.addNumbers("0736146716");
        agenda.addNumbers("0736146706");

        System.out.println(agenda.toString()); // apel explicit al metodei toString()
        System.out.println(agenda); // apel implicit al metodei toString()

       printNumber(agenda,"0736146750");
       printNumber(agenda,"0736146759");
       printNumber(agenda,"0736146751");
       printNumber(agenda,"0736146756");

       agenda.printAgenda();
    }

    // metoda static care ne permite reutilizarea codului de afisare a mesajului
    // !!! o metoda non-statica nu poate fi apelata dintr-un context static fara prezenta unui obiect din clasa in care ea este definita
    private static void printNumber(Agenda agenda, String numberToCheck) {
        if (agenda.search(numberToCheck)){
            System.out.println("Numarul a fost gasit in agenda ");
        }else{
            System.out.println("Numarul nu a fost gasit in agenda ");
        }
    }

}
