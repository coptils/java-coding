package com.sda.fundamentals.agenda;

import java.util.Arrays;

public class Agenda {
    private static final int MAX_CAPACITY = 10; // cu final marcam o constata in Java; numele constantelor se scriu cu litera mare si se despart cuvintele cu _
    // MAX_CAPACITY este o variabila de clasa deoarece e marcata cu static;

    // urmatoarele doua variabile, sunt variabile de instanta; ele sunt specifice fiecarei instante in parte
    private String[] telephoneNumbers = new String[MAX_CAPACITY];
    private int contor = 0;

    public void addNumbers(String number) { // metoda non-statica => avem nevoie de un obiect din clasa Agenda pentru a o putea apela
        if (contor == MAX_CAPACITY) {
            System.out.println("Agenda is full ");
            return;
        }
        telephoneNumbers[contor++] = number; // se incrementeaza si contorul dupa ce a fost folosita valoarea lui curenta
        //contor++;
    }

    public boolean search(String telephoneNumber) {
        for (int i = 0; i < contor; i++) {
            if (telephoneNumber.equals(telephoneNumbers[i])) {
                return true;
            }
        }
        return false;

    }

    public void printAgenda() {
        //for (String element : telephoneNumbers){  // enhanced for parcurge toate elementele dintr-un array
        for (int i = 0; i < contor; i++) {
            System.out.println(telephoneNumbers[i]);
        }
    }

    @Override
    public String toString() { // metoda auto-generata cu SHIFT -> INSERT
        return "Agenda{" +
                "telephoneNumbers=" + Arrays.toString(telephoneNumbers) +
                '}';
    }

}

