package com.sda.fundamentals.randomWord;

import java.util.Random;

/**
 * Write an application that consists of few classes:
 * a. Author class, representing an author – poem writer, which consists of fields surname and nationality (both of type String)
 * b. Poem class, representing poem, which consists of fields creator (type Author) and stropheNumbers (type int – numbers of strophes in poem)
 * c. Main class, with main method, inside which you will:
 * i. Create three instances of Poem class, fill them with data (using constructor and/or setters) and store them in array
 * ii. Write a surname of an author, that wrote a longest poem (let your application calculate it!)
 */
public class Main {
    public static void main(String[] args) {

        String[] randomWords = new String[]{
                generateRandomWord(5),
                generateRandomWord(3),
                generateRandomWord(10),
                generateRandomWord(7)
        };

        for (String randomWord : randomWords) {
            System.out.println(randomWord);
        }

    }

    private static String generateRandomWord(int length) {
        Random random = new Random();
        String randomString = "";

        for (int i = 0; i < length; i++) {
            char randomChar = (char) random.nextInt(97, 122); // 97 = codul asccii 'a', 122 = codul ascii 'z'
            randomString += randomChar;
        }

        return randomString;
    }


    public static void arrayLongestChar() {
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        String[] value = new String[10];
        Random random = new Random();
        String max = "";
        int length = 0;
        for (int i = 0; i < value.length; i++) {
            String word = "";
            int wordLenght = random.nextInt(alphabet.length());

            for (int j = 0; j < wordLenght; j++) {
                word = word + alphabet.charAt(random.nextInt(alphabet.length()));
            }
            value[i] = word;

            if (word.length() > length) {
                max = word;
                length = word.length();
            }
        }

        for (int i = 0; i < value.length; i++) {
            System.out.println(value[i]);
        }

        System.out.println("max  : " + max);
    }
}
