package com.sda.fundamentals.petrol.service;

import com.sda.fundamentals.petrol.model.Driver;
import com.sda.fundamentals.petrol.model.Pump;

import java.util.Scanner;

public class GasStation {

    private double fuelPrice;

    public GasStation(double fuelPrice) {
        this.fuelPrice = fuelPrice;
    }

    public double calculateTotalPrice(Pump pump) {
        return fuelPrice * pump.getTotalAmountOfFuel();
    }

    public void processPayment(Driver driver, Pump pump) {
        double priceToPay = this.calculateTotalPrice(pump);

        Scanner scanner = new Scanner(System.in);
        System.out.printf("Dear %s, you have to pay a total price of: %.2f RON.%n", driver, priceToPay);

        while (true) {
            System.out.println("Please, enter the money you want to pay: ");
            double enteredAmount = scanner.nextDouble();
            driver.setAccount(enteredAmount);
            double result = driver.pay(priceToPay);

            if (result == 0.0) {
                break;
            } else if (result > 0.0) {
                System.out.printf("%s, here is your change: %.2f RON%n", driver, result);
                break;
            } else {
                priceToPay = result * (-1);
                System.out.printf("%s, you still have to pay %.2f RON.%n", driver, priceToPay);
            }
        }

        System.out.println("Thanks for your payment! See you next time, " + driver + "!");
    }
}
