package com.sda.fundamentals.petrol.model;

public class Driver {
    private String name;
    private double account;

    public Driver(String name) {
        this.name = name;
    }

    public void setAccount(double account) {
        this.account = account;
    }

    public double pay(double price) {
        return Math.round(account - price);
    }

    @Override
    public String toString() {
        return name;
    }
}
