package com.sda.fundamentals.petrol.model;

import java.util.Random;

public class Pump {

    private static final Random RANDOM = new Random();
    private double totalAmountOfFuel;

    public Pump() {
        this.totalAmountOfFuel = 0;
    }

    public Pump pump() {
        this.totalAmountOfFuel += RANDOM.nextDouble(1.0, 10.0);
        return this;
    }

    public double getTotalAmountOfFuel() {
        return this.totalAmountOfFuel;
    }

}
