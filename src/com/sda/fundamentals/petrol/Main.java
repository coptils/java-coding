package com.sda.fundamentals.petrol;

import com.sda.fundamentals.petrol.model.Driver;
import com.sda.fundamentals.petrol.model.Pump;
import com.sda.fundamentals.petrol.service.GasStation;

import java.util.Scanner;

/**
 * Simulate the process of refueling. Within the while loop ask user if you should
 * continue or finish. For every entered “continue” command you should add a specific
 * amount of petrol and money (both of type double) and view it on the console.
 * At the end user should pay for petrol. Consider multiple possibilities, like:
 *
 * The user paid exactly as much as required.
 * The user paid too much (cashier should return the rest of the money).
 * The user paid too little – should be asked for the rest.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("Hello, driver!");
        System.out.println("Please, enter your name: ");
        Scanner scanner = new Scanner(System.in);
        Driver driver = new Driver(scanner.nextLine());
        GasStation omwStation = new GasStation(9.0);
        Pump pump = new Pump();

        while (true) {
            System.out.println(driver + ", should I 'continue' or 'finish' this process?");
            String option = scanner.nextLine();

            if (option.equals("finish")) {
                omwStation.processPayment(driver, pump);
                break;
            }

            if (option.equals("continue")) {
                System.out.printf("You have added %.2f l of fuel into your car!%n", pump.pump().getTotalAmountOfFuel());
                System.out.printf("You have to pay: %.2f RON!%n", omwStation.calculateTotalPrice(pump));
            }
        }
    }

}
