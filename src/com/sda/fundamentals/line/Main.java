package com.sda.fundamentals.line;

public class Main {
    public static void main(String[] args) {
        Point p1 = new Point(1, 2);
        Point p2 = new Point(1, 2);
        Line l12 = new Line(p1, p2);
        System.out.println(l12);
    }
}
