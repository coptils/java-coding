package com.sda.fundamentals.matrix;

/**
 * Example of a matrix:
 *         int a[][] = new int[][] {
 *                 {2, 3, 10, 4},
 *                 {3, 7, 1, 0},
 *                 {10, 9, 8, 3},
 *                 {5, 0, 3, 6}
 *         };
 *
 *  Create a class called 'Matrix' containing constructor that initializes the number of rows and number of columns of a new Matrix object.
 *
 *  The Matrix class has the following information:
 *  1. number of rows of matrix
 *  2. number of columns of matrix
 *  3. elements of matrix in the form of 2D array
 *
 *  The Matrix class has methods for each of the following:
 *  1 - get the number of rows
 *  2 - get the number of columns
 *  3 - set the elements of the matrix at given position (i,j)
 *  4 - adding two matrices. If the matrices are not addable, "Matrices cannot be added" will be displayed.
 *  5 - multiplying the two matrices
 */
public class Main {
    public static void main(String[] args) {
        Matrix m1 = new Matrix(3, 3);
        m1.setElement(0, 0, 1);
        m1.setElement(0, 1, 1);
        m1.setElement(0, 2, 1);
        m1.setElement(1, 0, 1);
        m1.setElement(1, 1, 1);
        m1.setElement(1, 2, 1);
        m1.setElement(2, 0, 1);
        m1.setElement(2, 1, 1);
        m1.setElement(2, 2, 1);

        System.out.println(m1);
        m1.add(m1);
        System.out.println(m1);
    }
}
