package com.sda.fundamentals.matrix;

public class Matrix {

    private int rows;
    private int columns;
    private int[][] elements;

    public Matrix(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        elements = new int[rows][columns];
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public void setElement(int row, int column, int element) {
        this.elements[row][column] = element;
    }

    public int[][] getElements() {
        return elements;
    }

    public void add(Matrix matrix) {
        if (this.getRows() != matrix.getRows() || this.getColumns() != matrix.getColumns()) {
            System.out.println("Matrices cannot be added");
            return;
        }

        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getColumns(); j++) {
                this.elements[i][j] += matrix.getElements()[i][j];
            }
        }
    }

    @Override
    public String toString() {
        String str = "";

        for (int i = 0; i < getRows(); i++) {
            for (int j = 0; j < getColumns(); j++) {
                str += this.elements[i][j] + " ";
            }
            str += "\n";
        }


        return str;
    }
}
