package com.sda.fundamentals.retangle;

public class RetangleTest {
    public static void main(String[] args) {
        Retangle retangle1 = new Retangle(5,10); // un obiect din clasa Rectangle cu lungimea = 5 si latimea = 10
        System.out.println("Aria este "+ retangle1.getArea());
        System.out.println("Perimetru este "+ retangle1.getPerimeter());

    }
}
