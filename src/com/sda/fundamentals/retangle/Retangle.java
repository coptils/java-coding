package com.sda.fundamentals.retangle;

public class Retangle {
    private int length;
    private int width;

    public Retangle(int length, int width) {
        this.length = length;
        this.width = width;
    }

    public int getArea() {  // metoda nu este statica, avem nevoie de un obiect din aceasta clasa ca sa o putem apela
        return length * width;
    }

    public int getPerimeter(){
//        int perimeter = 2*length+2*width;
//        return perimeter;
        return 2*length + 2*width;
    }

}
