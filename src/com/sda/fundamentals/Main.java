package com.sda.fundamentals;  // packetul din care face parte clasa; este prima linie din fisierul .java

import java.util.Scanner; // importul clasei Scanner

// clasa publica
public class Main {

    // punctul de intrare al aplicatiei
    public static void main(String[] args) {
        System.out.println("\n\t\t\t\t\tMENU");
        System.out.println("1. Calculate the perimeter of a circle");
        System.out.println("2. Calculate BMI");
        System.out.println("3. Solve quadratic ecuation ");
        System.out.println("4. Display numbers ");
        System.out.println("5. Display prime numbers.");
        System.out.println("6. Display harmonic sum.");
        System.out.println("7. Calculator");
        System.out.println("8. Sum of digits");
        System.out.println("9. Revers number");
        System.out.println("10. Longest text");
        System.out.println("11.Space counter");
        System.out.print("\nPlease choose an option: ");

        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();

        switch (option) {
            case 1:
                calculateCirclePerimeter();
                break;
            case 2:
                calculateBMI();
                break;
            case 3:
                solveEcuation();
                break;
            case 4:
                displayNumbers();
                break;
            case 5:
                displayPrimeNumbers();
                break;
            case 6:
                calculateSum();
                break;
            case 7:
                calculator();
                break;
            case 8:
                sumOfDigits();
                break;
            case 9:
                reverseNumber();
                break;
            case 10:
                longestText();
                break;
            case 11:
                countSpaces();
                break;
            default:
                System.out.println("Option is not valid!");
        }
    }

    // perimeter = pi*D, where D is the diameter
    private static void calculateCirclePerimeter() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter the circle diameter (real number): ");
        float diametru = scanner.nextFloat();
        double perimetru = diametru * Math.PI;
        System.out.printf("\nPerimeter of the circle with diameter %5.2f is: %5.2f\n", diametru, perimetru);

    }

    // calculate BMI and checking if it's optimal or not
    private static void calculateBMI() {
        Scanner sc = new Scanner(System.in);

        System.out.print("\nIntroduce the weight (float number): ");
        float weight = sc.nextFloat();
        System.out.print("\nIntroduce the height (int number): ");
        int height = sc.nextInt();

        float height_meters = height / 100.0f;
        float bmi = weight / (height_meters * height_meters);

        if (bmi > 18.5 && bmi < 24.9) {        //optimal bmi >18.5 <24.9
            System.out.printf("\nBMI optimal - %f\n", bmi);
        } else {
            System.out.println("BMI not optimal");
        }
    }

    /**
     * Task 3
     * Write a program for solving a quadratic equation. The program should take three integers
     * (coefficients of the quadratic equation a, b, c) and calculate the roots of the
     * equation
     * If delta ∆ comes out negative, print "Delta negative" and exit the program.
     * Formulas you'll need:
     * delta = b^2 - 4ac
     * x1 = (-b - sqrt(delta)) / 2a
     * x2 = (-b + sqrt(delta)) / 2a
     */
    private static void solveEcuation() {
        Scanner scanner = new Scanner(System.in);
        System.out.printf("Introdu coeficientii : ");
        System.out.printf("A = ");
        int a = scanner.nextInt();
        System.out.printf("B = ");
        int b = scanner.nextInt();
        System.out.printf("C = ");
        int c = scanner.nextInt();

        int delta = b * b - 4 * a * c;
        if (delta < 0) {
            System.out.printf("Ecuatia nu are solutii.");
        } else {
            double x1 = (-b - Math.sqrt(delta)) / 2 * a;   // metoda sqrt din clasa Math calculeaza radicalul din numarul dat, in cazul nostru delta
            double x2 = (-b + Math.sqrt(delta)) / 2 * a;
            System.out.printf("\nRadacinile ecuatiei patratice sunt : x1 = %.2f si x2 = %.2f", x1, x2);
        }
    }

    /**
     * Task 4
     * Write an application that takes a positive number from the user (type int) and writes all
     * numbers from 1 to the given number, each on the next line, with the following changes:
     * ● in place of numbers divisible by 3, instead of a number the program should print "Fizz"
     * ● in place of numbers divisible by 7, instead of a number the program should write "Buzz"
     * ● if the number is divisible by both 3 and 7, the program should print "Fizz buzz"
     */
    private static void displayNumbers() {
        int number = getPositiveNumber();
        for (int i = 1; i <= number; i++) {

            // in cazul in care avem un if inlantuit ca in exemplul de mai jos, prima conditie trebuie sa fie cea mai restrictiva
            // punem conditiile astfel incat fiecare dintre ele sa poata fi atinse la un moment dat;
            // prima o sa fie conditia cea mai restrictiva, iar ultima cea mai putin restrictiva; de la mic la larg/mare
            if (i % 3 == 0 && i % 7 == 0) {
                System.out.println("Fizz, Buzz");
            } else if (i % 3 == 0) {
                System.out.println("Fizz");
            } else if (i % 7 == 0) {
                System.out.println("Buzz");
            } else {
                System.out.println(i);
            }
        }
    }

    /**
     * Task 5
     * Write an application that takes a positive number from the user (type int) and prints all
     * prime numbers greater than 1 and less than the given number.
     */
    private static void displayPrimeNumbers() {
        int number = getPositiveNumber();
        for (int i = 2; i < number; i++) {
            if (isPrime(i)) {
                System.out.println(i);
            }
        }
    }

    // What is a prime number?
    // un numar natural > 1, care are exact doi divizori pozitivi, numarul 1 si el insusi
    private static boolean isPrime(int numberToCheck) {
//        for (int i = 2; i < numberToCheck; i++) {  // aici puteam merge cu verificarea doar pana la jumatatea numarului numberToCheck ca si in exemplul urmator
        for (int i = 2; i <= Math.sqrt(numberToCheck); i++) {  // daca nu am gasit numere divizibile cu numarul insusi pana la jumatatea lui, atunci nu o sa gasim nici in cealalta jumatate :)
            // incepem de la 2 cu counter ul deoarece orice numar ar fi divizibil cu 1 si atunci metoda ar returna tot timpul false
            if (numberToCheck % i == 0) {
                return false;
            }
        }
        return true;
    }


    // we have extracted the duplicated code into a method and reused the code by calling the method
    // we have made the methods static because a non-static method cannot be called from another static method
    private static int getPositiveNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a positive number: ");
        int number = scanner.nextInt();
        while (number < 0) { // atata timp cand numarul introdus de catre utilizator nu este positiv, cerem sa numbarul sa fie re-introdus
            System.out.println("Your number is not positive, please enter a new one");
            number = scanner.nextInt();
        }
        return number;
    }

    /**
     * Write an application that takes a number n from the user (type int) and calculates the
     * sum of the harmonic series from 1 to n, according to the formula below:
     * Hn = 1/1 + 1/2 + 1/3 + 1/4 + ... + 1/n
     */
    private static void calculateSum() {
        int n = getPositiveNumber();  // reutilizam metoda deja existenta pentru a citi un numar de la tastatura
        double sum = 0.0d;
        for (double i = 1.0; i <= n; i++) {
            sum += 1 / i; // daca i-ul nu era numar real, adunam tot timpul un numar intreg la suma => pierdere de date
        }
        System.out.printf("\nHarmonic sum from 1 to %d is %f\n", n, sum);

    }

    /**
     * Write an application that implements a simple calculator. The application should:
     * a. read first number (type float)
     * b. read one of following symbols: + - / *
     * c. read second number (type float)
     * d. return a result of given mathematical operation
     * If the user provides a symbol other than supported, the application should print "Invalid
     * symbol". If the entered action cannot be implemented (i.e. it is inconsistent with the
     * principles of mathematics), the application should print "Cannot calculate".
     */
    private static void calculator() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Introduceti primul nr: ");
        float numar1 = sc.nextFloat();
        System.out.println("Introduceti al doilea nr: ");
        float numar2 = sc.nextFloat();
        System.out.println("Intoduceti operatia + - / * ");
        sc = new Scanner(System.in); // reinitializam scanner ul, altfel ar fi luat in cosiderare \n ca si string introdus
        String simbol = sc.nextLine();

        // pentru a compara string-uri, folosim metoda .equals()
        if (numar2 == 0 && simbol.equals("/")) {
            System.out.println("Cannot calculate");
            return;  // aici se iese din metoda, liniile de cod urmatoare nu se mai executa;
        }
        float rezultat = 0.0f;
        switch (simbol) {
            case "+":
                rezultat = numar1 + numar2;
                break;
            case "-":
                rezultat = numar1 - numar2;
                break;
            case "/":
                rezultat = numar1 / numar2;
                break;
            case "*":
                rezultat = numar1 * numar2;
                break;
            default:
                System.out.println("Invalid simbol");


        }
        System.out.printf("%n  %f %s %f = %f %n", numar1, simbol, numar2, rezultat);


    }

    /**
     * Write an application that gets one positive number (type int) from the user and calculates
     * a sum of digits of the given number. Hint: to make some operations on every single digit
     * of the number (digit by digit), you can calculate the remainder of dividing the number by
     * 10 (to get the value of the last digit) and divide the number by 10 (to "move" to the next
     * digit).
     * 1234/10 = 123 rest 4
     * 123/10 =  12 rest 3
     * 12/10 =   1 rest 2
     * 1/ 10 =   0 rest 1
     */
    private static void sumOfDigits() {
        int number = getPositiveNumber();
        int tempNumber = number; // salvam numarul original intr-o variabila temporara, deoarece numarul original se altereaza la linia 275
        int sum = 0;
        while (number != 0) { // atatat timp cat nu am ajuns cu catul impartirii la 0, tot ipartim catul la 10 si adunam restul la suma
            sum = sum + number % 10; // tot luam restul impartirii la 10 care ne descompune numarul de la coada la cap;
            number = number / 10; // avem nevoie de cat sa il refolosim la urmatoarea iteratie
        }
        System.out.printf("\nSuma cifrelor numarului %d este %d\n", tempNumber, sum);

    }

    private static void reverseNumber() {
        int number = getPositiveNumber();
        int tempNumber = number;
        String reversNumber = "";
        while (number != 0) {
            reversNumber = reversNumber + number % 10; // concatenam intr-un string numarul de la final la cap, cifra dupa cifra
            number = number / 10; // dupa ce am luat ultima cifra din numar, diminuam numarul, refolosim catul pentru ca mai sus sa obtinem penultima cifra din nr ....
        }
        int revers = Integer.valueOf(reversNumber); // asa convertim un string in numar intreg
        System.out.printf("\nRevers of %d este %d\n", tempNumber, revers);

    }

    /**
     * Write an application that will read texts (variables of the String type) until the user gives
     * the text "Enough!" and then writes the longest of the given texts (not including the text
     * "Enough!"). If the user does not provide any text, write "No text provided".
     */
    private static void longestText() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter as many texts as you want, and finish with \"Enough!\"");
        String text = sc.nextLine();
        // daca utilizatorul a introdus direct cuvantul de final, atunci afisam un mesaj
        if (text.equals("Enough!")) {
            System.out.println("No text provided");
            return;
        }
        String longestTxt = text; // pentru inceput, cel mai lung numar este primul numar citi
        while (!text.equals("Enough!")) { // atata timp cat text nu este egal cu "Enough!", citim texte noi
            text = sc.nextLine();
            // text.length() => returneaza lungimea unui string
            if (text.length() > longestTxt.length()) { // daca in timp ce citim alt text de la tastatura gasim unul mai lung, longestTxt o sa primeasca acea valoare
                longestTxt = text;
            }
        }
        System.out.printf("\nThe longest text entered was: %s\n", longestTxt);
    }

    /**
     * Write an application that reads a text from the user (type String) and counts occurrences of a space character.
     */
    private static void countSpaces(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Introduceti un text: ");
        String stringCitit = sc.nextLine();
        int spaceCounter = 0; // cu aceasta variabila numaram spatiile

        for(int i = 0; i < stringCitit.length(); i++) {
            String char1 = stringCitit.substring(i,i+1); // luam pe rand cate o litera din text; substring(0,1) => prima litera; substring(1,2) => a doua litera ....
            if(char1.contains(" ")){ // daca o litera contine spatiu, atunci incrementam numarul de aparitii al spatiilor
                spaceCounter++;

            }
        }

        System.out.println("Numarul de spatii este: " + spaceCounter);
    }
}

