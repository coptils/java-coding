package com.sda.fundamentals;

public class Test {
    public static void main(String[] args) {
        int a[][] = new int[][] {
                {2, 3, 10, 4},
                {3, 7, 1, 0},
                {10, 9, 8, 3},
                {5, 0, 3, 6}
        };

        System.out.println(sumOfMainDiag(a));
        System.out.println(sumOfSecondDiag(a));
        sumOfDiagonals(a);
        System.out.println(compressedString("aabcccccaa"));
    }

    private static int sumOfMainDiag(int matrix[][]) {
        int sum = 0;
        for (int row = 0; row < matrix.length; row++) {
            for (int column = 0; column < matrix[row].length; column++) {
                if (row == column) {
                    sum += matrix[row][column];
                }
            }
        }
        return sum;
    }

    private static int sumOfSecondDiag(int matrix[][]) {
        int sum = 0;
        int noToCheck = matrix.length - 1;
        for (int row = 0; row < matrix.length; row++) {
            for (int column = matrix[row].length - 1; column >= 0; column--) {
                if (noToCheck == column) {
                    sum += matrix[row][column];
                    noToCheck--;
                }
            }
        }
        return sum;
    }

    // complexity: O(n)
    private static void sumOfDiagonals(int matrix[][]) {
        int length = matrix.length;
        int principalSum = 0;
        int secondarySum = 0;
        for (int i = 0; i < length; i++) {
            principalSum += matrix[i][i];
            secondarySum += matrix[i][length - i - 1];
        }
        System.out.println("Main diagonal sum is: " + principalSum);
        System.out.println("Secondary diagonal sum is: " + secondarySum);
    }

    private static String compressedString(String str) {
        String finalString = "";
        int currentIndex = 0;

        while(currentIndex < str.length()) {
            int count = 1;
            while (currentIndex + 1 < str.length() && str.charAt(currentIndex) == str.charAt(currentIndex + 1)) {
                count++;
                currentIndex++;
            }
            finalString += str.charAt(currentIndex) + "" + count;
            currentIndex++;
        }

        return finalString;
    }

}