package com.sda.fundamentals.dates;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.util.Scanner;

public class TestDates {
    public static void main(String[] args) {
        LocalDate currentDate = LocalDate.now(); // un obiect care contine data curenta
        System.out.println(currentDate);
        LocalDate date1 = LocalDate.of(1970, 01, 01);  // data custom; 1970-01-01
        System.out.println(date1);
        System.out.println(date1.getDayOfMonth() - currentDate.getDayOfMonth());

        LocalTime now = LocalTime.now(); // asa obtinem timpul curent al sistemului
        System.out.println(now);

        LocalDateTime dateTime = LocalDateTime.now(); // timpul si data curenta
        System.out.println(dateTime);

        LocalDate currDate = LocalDate.parse("2021-01-01"); // asa cream un obiect de tipul LocalDate dintr-un date string
        System.out.println(currDate);

        String dateString = "2021-08-16";
        LocalDate courseDate = LocalDate.parse(dateString); // transformam string date ul intr-un obiect LocalDate ca sa ne fie mai usor sa manipulam data

        Period period =  Period.between(currentDate, courseDate); // variabila de tipul Period; metoda between calculeaza perioada dintre cele doua date pasate ca si argumente metodei
        System.out.println(period.getDays()); // afisam numarul de zile al perioadei
    }
}
