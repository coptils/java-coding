package com.sda.fundamentals.discount;

import com.sda.fundamentals.discount.model.Customer;
import com.sda.fundamentals.discount.model.Visit;
import com.sda.fundamentals.discount.service.SaloonService;

import java.time.LocalDate;

/**
 * You are asked to write a discount system for a beauty saloon, which provides services and sells beauty products.
 * It offers 3 types of memberships: Premium, Gold and Silver.
 * Premium, gold and silver members receive a discount of 20%, 15%, and 10%, respectively, for all services provided.
 * Customers without membership receive no discount.
 * All members receives a flat 10% discount on products purchased (this might change in future).
 * Your system shall consist of three classes: Customer, Discount and Visit, as shown in the class diagram.
 * It shall compute the total bill if a customer purchases $x of products and $y of services, for a visit.
 */
public class Main {
    public static void main(String[] args) {
        SaloonService saloonService = new SaloonService();

        Customer customer1 = new Customer("Ana");
        customer1.setMember(true);
        customer1.setMemberType("Silver");
        Visit visit = new Visit(customer1, LocalDate.now());
        visit.setServiceExpense(100.0);
        visit.setProductExpense(10.0);

        Customer customer2 = new Customer("Andrei");
        customer2.setMember(false);
        Visit visit2 = new Visit(customer2, LocalDate.now());
        visit2.setServiceExpense(100.0);
        visit2.setProductExpense(10.0);

        System.out.println(saloonService.computePrice(visit));
        System.out.println(saloonService.computePrice(visit2));

    }
}
