package com.sda.fundamentals.discount.service;

import com.sda.fundamentals.discount.model.DiscountRate;
import com.sda.fundamentals.discount.model.Visit;

public class SaloonService {

    private DiscountRate discountRate;

    public SaloonService() {
        discountRate = new DiscountRate();
    }

    public double computePrice(Visit visit) {
        if (visit.getCustomer().isMember()) {
            return discountRate.getServiceDiscountRate(visit.getCustomer().getMemberType()) * visit.getServiceExpense() +
                    discountRate.getProductDiscountRate(visit.getCustomer().getMemberType()) * visit.getProductExpense();
        }

        return visit.getTotalExpense();
    }

}
