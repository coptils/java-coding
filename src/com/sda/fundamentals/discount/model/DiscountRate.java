package com.sda.fundamentals.discount.model;

public class DiscountRate {
    private static final double serviceDiscountPremium = 0.2;
    private static final double serviceDiscountGold = 0.15;
    private static final double serviceDiscountSilver = 0.1;
    private static final double productDiscountPremium = 0.1;
    private static final double productDiscountGold = 0.1;
    private static final double productDiscountSilver = 0.1;

    public double getServiceDiscountRate(String type) {
        double discountRate = 1.0;
        switch (type) {
            case "Premium":
                discountRate = serviceDiscountPremium;
                break;
            case "Gold":
                discountRate = serviceDiscountGold;
                break;
            case "Silver":
                discountRate = serviceDiscountSilver;
                break;
        }
        return discountRate;
    }

    public double getProductDiscountRate(String type) {
        double discountRate = 1.0;
        switch (type) {
            case "Premium":
                discountRate = productDiscountPremium;
                break;
            case "Gold":
                discountRate = productDiscountGold;
                break;
            case "Silver":
                discountRate = productDiscountSilver;
                break;
        }
        return discountRate;
    }
}
